package com.algorithm.node;

import com.algorithm.node.Node;
import org.junit.Test;

/**
 * @Author _G5niusX
 * @Date 2018/4/21 4:48
 */
public class NodeTest {

    @Test
    public void nodeTest() {
        Node<Integer> head = new Node<>(0);
        for (int i = 0; i < 20; i++) {
            head.addData(i);
        }
        System.out.print(head);
    }

}
