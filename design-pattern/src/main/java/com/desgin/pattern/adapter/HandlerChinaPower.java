package com.desgin.pattern.adapter;

/**
 * 持有对象的适配器
 */
public class HandlerChinaPower implements CPower {

    private APower aPower;

    public APower getaPower() {
        return aPower;
    }

    public void setaPower(APower aPower) {
        this.aPower = aPower;
    }

    @Override
    public void chinaCharge() {
        aPower.charge();
    }
}
