package com.algorithm.tree;

/**
 * @author SaErNuoSi
 * @date 2018/4/11 12:58
 */
public class BTree {
    private TreeNode treeNode;

    public TreeNode getTreeNode() {
        return treeNode;
    }

    public void setTreeNode(TreeNode treeNode) {
        this.treeNode = treeNode;
    }
}
