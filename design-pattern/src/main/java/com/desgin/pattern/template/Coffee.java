package com.desgin.pattern.template;

import lombok.extern.log4j.Log4j2;

/**
 * 制作咖啡
 *
 * @author SaErNuoSi
 * @date 2018/4/27 16:05
 */
@Log4j2
public class Coffee extends AbstractCaffeineBeverage {
    @Override
    protected void brew() {
        log.info("咖啡正在烹煮....");
    }

    @Override
    protected void addCondiments() {
        log.info("正在给咖啡加料....");
    }
}
