package com.algorithm.math;

import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Test;

/**
 * @Author _G5niusX
 * @Date 2018/4/27 11:45
 */
@Log4j2
public class MathUtils {

    @Test
    public void absTest() {
        int num = -90;
        Assert.assertEquals(XMathUtils.abs(num), Math.abs(num));
        log.info(XMathUtils.abs(-2147483648));
    }
}
