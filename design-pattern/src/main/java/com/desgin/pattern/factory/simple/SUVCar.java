package com.desgin.pattern.factory.simple;

import com.desgin.pattern.factory.Car;

/**
 * @author SaErNuoSi
 * @date 2018/4/13 14:20
 */
public class SUVCar implements Car {
    @Override
    public String name() {
        return "Urus";
    }

    @Override
    public int speed() {
        return 30000;
    }
}
