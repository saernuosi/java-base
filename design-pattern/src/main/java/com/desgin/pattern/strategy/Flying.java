package com.desgin.pattern.strategy;

/**
 * 飞行接口.给鸭子飞行的能力
 *
 * @Author _G5niusX
 * @Date 2018/4/22 13:58
 */
@FunctionalInterface
public interface Flying {
    void fly();
}
