package com.desgin.pattern.iterator;

import org.junit.Test;

/**
 * @Author _G5niusX
 * @Date 2018/5/1 17:34
 */
public class IteratorTest {

    @Test
    public void complexTest() {
        PancakeHouseMenu pancakeHouseMenu = new PancakeHouseMenu();
        DinerMenus dinerMenus = new DinerMenus();
        Waitress waitress = new Waitress(pancakeHouseMenu, dinerMenus);
        waitress.printMenus();
    }
}
