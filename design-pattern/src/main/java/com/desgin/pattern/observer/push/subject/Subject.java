package com.desgin.pattern.observer.push.subject;

import com.desgin.pattern.observer.push.observer.Observer;

/**
 * 主题接口,它的实现类会通知它的订阅者
 *
 * @Author _G5niusX
 * @Date 2018/4/24 18:52
 */
public interface Subject {
    /**
     * 注册观察者
     */
    void registerObserver(Observer observer);

    /**
     * 移除观察者
     */
    void removeObserver(Observer observer);

    /**
     * 通知观察者
     */
    void notifyObserver();
}
