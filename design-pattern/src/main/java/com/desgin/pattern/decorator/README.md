## 装饰者模式
### 装饰者模式的概念
> 装饰者模式将责任动态附加到了对象上,若要扩展功能装饰者提供了比继承更好的弹性,JAVA中的IO流操作就是使用了装饰者模式,本文会尝试做一杯冰摇柠檬茶

----------

#### 1.1 JAVA代码
> 先创建一个饮料的抽象类,把计算价格的方法让子类去实现
```java
/**
 * 饮料基类
 *
 * @author SaErNuoSi
 * @date 2018/4/25 13:05
 */
public abstract class AbstractBeverage {

    protected String description;

    public String getDescription() {
        return description;
    }

    /**
     * 计算饮料的价格
     *
     * @return
     */
    abstract BigDecimal coast();
}
```
> 创建一个继承了抽象饮料的冰摇柠檬茶
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/25 13:57
 */
public class IceShakenLemonTea extends AbstractBeverage {
    public IceShakenLemonTea() {
        this.description = "冰摇柠檬茶";
    }

    @Override
    BigDecimal coast() {
        return new BigDecimal(15);
    }
}
```
> 创建抽象的一个调料类
```java
public abstract class CondimentDecorator extends AbstractBeverage {
    public abstract String getDescription();
}
```
> 创建一个具体的调料
```java
public class Whip extends CondimentDecorator {

    AbstractBeverage abstractBeverage;

    public Whip(AbstractBeverage abstractBeverage) {
        this.abstractBeverage = abstractBeverage;
    }

    @Override
    public String getDescription() {
        return "[奶泡]" + abstractBeverage.getDescription();
    }

    @Override
    BigDecimal coast() {
        return new BigDecimal(5).add(abstractBeverage.coast());
    }
}
```
#### 1.2 测试代码
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/19 17:22
 */
@Log4j2
public class DecoratorTest {

    @Test
    public void decoratorTest() {
        /*先生成冰摇柠檬茶*/
        IceShakenLemonTea iceShakenLemonTea = new IceShakenLemonTea();
        /*加摩卡*/
        Mocha mocha = new Mocha(iceShakenLemonTea);
        /*加奶泡*/
        Whip whip = new Whip(mocha);
        log.info("购买一杯:" + whip.getDescription() + "花费:" + whip.coast() + "$");
    }
}
```
### UML类图
![装饰者模式类图](https://img-blog.csdn.net/20180425143102501?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NhRXJOdW9TaQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
源码地址 [https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/decorator](https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/decorator)