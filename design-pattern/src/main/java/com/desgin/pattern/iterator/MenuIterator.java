package com.desgin.pattern.iterator;

/**
 * 菜单迭代器
 *
 * @Author _G5niusX
 * @Date 2018/5/1 17:21
 */
public interface MenuIterator {
    boolean hasNext();

    Object next();
}
