package com.desgin.pattern.builder;

/**
 * 建造者模式
 *
 * @author SaErNuoSi
 * @date 2018/4/13 17:01
 */
public class Computer {
    private String cpu;
    private String memory;
    private String display;
    private String graphicsCard;
    private String audio;

    public Computer(ComputerBuilder computerBuilder) {
        this.cpu = computerBuilder.cpu;
        this.memory = computerBuilder.memory;
        this.display = computerBuilder.display;
        this.graphicsCard = computerBuilder.graphicsCard;
        this.audio = computerBuilder.audio;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getGraphicsCard() {
        return graphicsCard;
    }

    public void setGraphicsCard(String graphicsCard) {
        this.graphicsCard = graphicsCard;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "cpu='" + cpu + '\'' +
                ", memory='" + memory + '\'' +
                ", display='" + display + '\'' +
                ", graphicsCard='" + graphicsCard + '\'' +
                ", audio='" + audio + '\'' +
                '}';
    }

    public static class ComputerBuilder {
        private String cpu;
        private String memory;
        private String display;
        private String graphicsCard;
        private String audio;

        public ComputerBuilder withCpu(String cpu) {
            this.cpu = cpu;
            return this;
        }

        public ComputerBuilder withMemory(String memory) {
            this.memory = memory;
            return this;
        }

        public ComputerBuilder withDisplay(String display) {
            this.display = display;
            return this;
        }

        public ComputerBuilder withGraphicsCard(String graphicsCard) {
            this.graphicsCard = graphicsCard;
            return this;
        }

        public ComputerBuilder withAudio(String audio) {
            this.audio = audio;
            return this;
        }

        public Computer build() {
            return new Computer(this);
        }
    }
}
