package com.desgin.pattern.template;

import lombok.extern.log4j.Log4j2;

/**
 * @author SaErNuoSi
 * @date 2018/4/27 15:58
 */
@Log4j2
public abstract class AbstractCaffeineBeverage {
    final void prepareRecipe() {
        boilWater();
        brew();
        addCondiments();
    }

    /**
     * 做饮料的具体方法
     */
    protected abstract void brew();

    /**
     * 加调味品
     */
    protected abstract void addCondiments();

    void boilWater() {
        log.info("烧开水...");
    }
}
