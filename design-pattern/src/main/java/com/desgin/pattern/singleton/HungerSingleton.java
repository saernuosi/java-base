package com.desgin.pattern.singleton;

/**
 * 立即加载的方式,最简单有效的方法
 *
 * @Author _G5niusX
 * @Date 2018/4/22 18:16
 */
public class HungerSingleton {
    private static final HungerSingleton HUNGER_SINGLETON = new HungerSingleton();

    /**
     * 构造方法私有化
     */
    private HungerSingleton() {

    }

    /**
     * 对外暴漏的方法
     *
     * @return
     */
    public static HungerSingleton instance() {
        return HUNGER_SINGLETON;
    }
}
