package com.desgin.pattern.proxy.statics;

import com.desgin.pattern.proxy.Handler;

/**
 * 静态代理模式
 */
public class ProxyHandler implements Handler {

    private Handler handler;

    public ProxyHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public String doSomething(String string) {
        return handler.doSomething(string);
    }
}
