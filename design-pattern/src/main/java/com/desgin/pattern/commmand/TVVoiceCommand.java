package com.desgin.pattern.commmand;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 15:36
 */
public class TVVoiceCommand implements Command {
    TV tv;
    int voice;

    public TVVoiceCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        /*音量每次加10*/
        tv.setVoice((voice = voice + 10));
    }

    @Override
    public void undo() {
        tv.setVoice((voice = voice - 10));
    }
}
