# static关键字
* static的作用

> 被static修改的类或者方法或者变量(句柄)可以被直接调用,同时被static修饰的变量在内存中只有一份

------------

> 在StaticApplication中申明一个static的i,初始值为90,每次构造的时候都会+1,并且打印当前i的值
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/26 21:25
 */
public class StaticApplication {
    public static int i = 90;

    public StaticApplication() {
        i++;
        System.out.println(this + "中i的值:" + i);
    }
}
```
---
> 测试类中建立2个静态代码块,并且输出日志,在main方法中连续创建2个对象,最后看输出的i
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/26 21:26
 */
public class StaticApplicationTest {
    static {
        int i = StaticApplication.i;
        //i = 90
        System.out.println("测试方法静态代码块中的i:" + i);
    }

    static {
        System.out.println("申明的第二个静态代码块");
    }

    public static void main(String[] args) {
        // i = 91
        new StaticApplication();
        // i = 92
        new StaticApplication();
    }
}
```
> 这个结果证明了,2个类都在操作同一个变量i,而这个变量是被static所修饰的.

源码地址 [https://gitee.com/saernuosi/java-base/tree/master/java-core/src/main/java/com/core/statics](https://gitee.com/saernuosi/java-base/tree/master/java-core/src/main/java/com/core/statics)