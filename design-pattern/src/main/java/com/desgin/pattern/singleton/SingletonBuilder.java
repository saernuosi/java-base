package com.desgin.pattern.singleton;

/**
 * 单例模式
 *
 * @author SaErNuoSi
 * @date 2018/4/13 16:37
 */
public class SingletonBuilder {


    private SingletonBuilder() {

    }

    public static Singleton getInstance() {
        return SingleFactory.getSingleIoc();
    }

    private static class SingleFactory {
        private static final Singleton SINGLETON = new Singleton();

        public static Singleton getSingleIoc() {
            return SingleFactory.SINGLETON;
        }
    }
}
