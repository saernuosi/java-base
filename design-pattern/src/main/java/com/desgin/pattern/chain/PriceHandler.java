package com.desgin.pattern.chain;

/**
 * 责任链的抽象,通过持有一个自己的对象,来形成链表似的数据结构
 *
 * @Author _G5niusX
 * @Date 2018/4/22 16:36
 */
public abstract class PriceHandler {
    protected String name;
    protected PriceHandler nextPriceHandler;

    public PriceHandler(String name) {
        this.name = name;
    }

    abstract void processPrice(float discount);

    /**
     * 创建了一个[销售人员]->[销售经理]->[CEO]的责任链
     *
     * @return
     */
    public static PriceHandler createPriceHandler() {
        PriceHandler salesPriceHandler = new SalesPriceHandler("销售人员");
        ManagerPriceHandler managerPriceHandler = new ManagerPriceHandler("销售经理");
        CEOPriceHandler ceoPriceHandler = new CEOPriceHandler("CEO");
        /*重点在于给这些实例指明谁是下一个处理者*/
        salesPriceHandler.nextPriceHandler = managerPriceHandler;
        managerPriceHandler.nextPriceHandler = ceoPriceHandler;
        /*最后返回一个权限最低的销售人员出去*/
        return salesPriceHandler;
    }
}
