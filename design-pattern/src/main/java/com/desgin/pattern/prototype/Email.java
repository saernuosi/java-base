package com.desgin.pattern.prototype;

import java.io.*;

/**
 * @author SaErNuoSi
 * @date 2018/4/13 20:27
 */
public class Email implements Cloneable {

    private String subject;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Email clone = (Email) super.clone();
        return clone;
    }

    /**
     * 使用二进制的深拷贝
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Email deepClone() throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(this);

        /* 读出二进制流产生的新对象 */
        ByteArrayInputStream bis = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        return (Email) ois.readObject();
    }

    @Override
    public String toString() {
        return "Email{" +
                "subject='" + subject + '\'' +
                '}';
    }
}
