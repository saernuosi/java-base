package com.desgin.pattern.chain;

/**
 * 客户请求折扣是否可行
 * @Author _G5niusX
 * @Date 2018/4/22 16:50
 */
public class Customer {

    private PriceHandler priceHandler;


    public Customer(PriceHandler priceHandler) {
        this.priceHandler = priceHandler;
    }

    public void requestDiscount(float discount) {
        priceHandler.processPrice(discount);
    }
}
