package com.desgin.pattern.observer.push.observer;

import com.desgin.pattern.observer.DisplayElement;
import com.desgin.pattern.observer.WeatherData;
import com.desgin.pattern.observer.push.subject.Subject;

import java.text.MessageFormat;

/**
 * 当前的天气布告板
 *
 * @Author _G5niusX
 * @Date 2018/4/24 19:29
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {

    private Subject weatherSubject;
    private WeatherData weatherData;
    private String name;

    public CurrentConditionsDisplay(String name, Subject weatherSubject) {
        this.name = name;
        this.weatherSubject = weatherSubject;
        /*在进行实例化的时候,将这个观察者注册到订阅者里面*/
        this.weatherSubject.registerObserver(this);
    }

    @Override
    public void display() {
        String template = this.name + "播报当前的天气温度是: {0}°C, 湿度是: {1}%, 压强是: {2}Pa";
        System.out.println(MessageFormat.format(template, weatherData.getTemperature(), weatherData.getHumidity(), weatherData.getPressure()));
    }

    /**
     * 订阅者会调用这个方法来通知观察者天气数据的改变
     *
     * @param weatherData
     */
    @Override
    public void update(WeatherData weatherData) {
        this.weatherData = weatherData;
        display();
    }
}
