package com.desgin.pattern.factory.simple;

import com.desgin.pattern.factory.Car;

/**
 * @author SaErNuoSi
 * @date 2018/4/13 14:13
 */
public class SpecialCar implements Car {
    @Override
    public String name() {
        return "特种车";
    }

    @Override
    public int speed() {
        return 1000;
    }


}
