package com.desgin.pattern.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author _G5niusX
 * @Date 2018/5/1 17:08
 */
public class PancakeHouseMenu {
    private List<MenuItem> menuItemArrayList;

    public PancakeHouseMenu() {
        menuItemArrayList = new ArrayList<>();
        addItem("菠萝面包", "使用菠萝和奶油做的面包", 20);
        addItem("草莓脏脏包", "加了草莓的脏脏包", 13);
        addItem("切片吐司面包", "吐司面包", 15);
    }

    private void addItem(String name, String description, double price) {
        MenuItem menuItem = new MenuItem(name, description, price);
        menuItemArrayList.add(menuItem);
    }

    public MenuIterator iterator() {
        return new PancakeHouseMenuIterator(menuItemArrayList);
    }


}
