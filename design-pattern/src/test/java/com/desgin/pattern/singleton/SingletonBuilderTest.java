package com.desgin.pattern.singleton;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author SaErNuoSi
 * @date 2018/4/13 16:43
 */
@Log4j2
public class SingletonBuilderTest {

    @Test
    public void singleTest() throws InterruptedException {
        Singleton instance = SingletonBuilder.getInstance();
        System.out.println(instance);
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                Singleton instance1 = SingletonBuilder.getInstance();
                log.info(Thread.currentThread().getName() + "|" + instance1);
            }).start();
        }
        TimeUnit.SECONDS.sleep(10);
    }
}
