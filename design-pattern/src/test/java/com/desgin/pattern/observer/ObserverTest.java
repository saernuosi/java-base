package com.desgin.pattern.observer;

import com.desgin.pattern.observer.pull.WeatherObservable;
import com.desgin.pattern.observer.push.observer.CurrentConditionsDisplay;
import com.desgin.pattern.observer.push.subject.WeatherSubject;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

/**
 * @Author _G5niusX
 * @Date 2018/4/24 19:36
 */
@Log4j2
public class ObserverTest {

    private static final String[] NAMES = new String[5];

    @Before
    public void init() {
        for (int i = 0; i < NAMES.length; i++) {
            NAMES[i] = "CCTV " + (i + 1);
        }
    }

    @Test
    public void weatherPushObserverTest() {
        for (int i = 0; i < NAMES.length; i++) {
            /*先创建消息订阅者*/
            WeatherSubject weatherSubject = new WeatherSubject();
            /*创建当前的天气,将它传递给订阅者*/
            WeatherData weatherData = new WeatherData();
            float x = new Random().nextFloat();
            x = (float) Math.round(x * 100);
            weatherData.setTemperature(x);
            weatherData.setHumidity(x);
            weatherData.setPressure(x);
            /*创建消息观察者,并且将这个观察者注册到了订阅者里面*/
            CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(NAMES[i], weatherSubject);
            /*订阅者发布了消息*/
            weatherSubject.publishWeather(weatherData);
        }
    }

    @Test
    public void weatherPullObserverTest() {
        WeatherObservable weatherObservable = new WeatherObservable();
        weatherObservable.registerObserver((weatherObserver, object) -> {
            float temperature = weatherObservable.getTemperature();
            System.out.println("CCTV播报温度:" + temperature);
        });
        weatherObservable.registerObserver((weatherObserver, object) -> {
            float pressure = weatherObservable.getPressure();
            System.out.println("CCTV2播报压强:" + pressure);
        });
        weatherObservable.setWeatherData(20, 40, 80);
    }
}
