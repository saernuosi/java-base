package com.core.statics;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import java.util.Properties;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 21:26
 */
@Log4j2
public class StaticApplicationTest {
    static {
        /*输出系统信息*/
        Properties properties = System.getProperties();
        properties.list(System.out);
        Runtime runtime = Runtime.getRuntime();
        log.info("totalMemory:\t" + byte2MB(runtime.totalMemory()));
        log.info("maxMemory:\t" + byte2MB(runtime.maxMemory()));
        log.info("freeMemory:\t" + byte2MB(runtime.freeMemory()));
        int i = StaticApplication.i;
        //i = 90
        log.info("第一个静态代码块中的i:" + i);
    }

    static {
        log.info("第一个静态代码块中的i:" + StaticApplication.i);
    }


    @Test
    public void staticApplicationTest() {
        //i = 91
        new StaticApplication();
        //i = 92
        new StaticApplication();
        //i = 91
        new NotStaticApplication();
        //i = 91
        new NotStaticApplication();
    }

    /**
     * 二进制位运算符
     */
    @Test
    public void testBit() {
        int i = 1 << 0;
        int j = 1 << 1;
        log.info(Integer.toBinaryString(i));
        log.info(Integer.toBinaryString(j));
        /**
         * OR运算符
         * i | j
         * i      = 01
         * j      = 10
         * result = 11
         */
        log.info(Integer.toBinaryString(i | j));
        /**
         * AND运算符
         * i |& j
         * i      = 01
         * j      = 10
         * result = 00
         */
        log.info(Integer.toBinaryString(i & j));
        /**
         * 异或运算符
         * i |& j
         * i      = 01
         * j      = 10
         * result = 11
         */
        log.info(Integer.toBinaryString(i ^ j));
        /**
         * 对10取反
         * 00000000000000000000000000000010
         * 11111111111111111111111111111101
         */
        log.info(Integer.toBinaryString(~j));
    }

    static long byte2MB(long byteLength) {
        return byteLength / 1024 / 1024;
    }
}
