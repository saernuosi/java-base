## 命令模式
### 命令模式的概念
> 在面向对象程式设计的范畴中，命令模式（Command Pattern）是一种设计模式，它尝试以物件来代表实际行动
#### 1.1 JAVA代码
> 声明一个命令接口,有执行方法和撤销方法
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/26 13:44
 */
public interface Command {
    void execute();

    void undo();
}
```
> 声明灯行动的命令
```java
/**
 * 灯的命令类
 * @author SaErNuoSi
 * @date 2018/4/26 13:48
 */
public class LightOnCommand implements Command{
    Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.on();
    }

    @Override
    public void undo() {
        light.off();
    }
}
```
> 调用类
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/26 15:19
 */
public class RemoteController {
    Command[] onCommands;
    Command[] offCommands;
    Command cancelCommand;
    int length;

    public RemoteController(int length) {
        this.length = length;
        onCommands = new Command[length];
        offCommands = new Command[length];
        /*定义一个没有任何操作的命令,否则代码里面会有各种空判断*/
        Command noCommand = new NoCommand();
        for (int i = 0; i < length; i++) {
            onCommands[i] = noCommand;
            offCommands[i] = noCommand;
        }
        /*初始化的时候没有一个可以撤销的指令,所以用没有指令这个对象来替代*/
        cancelCommand = noCommand;
    }

    /**
     * 设置命令的执行顺序
     *
     * @param position
     * @param onCommand
     * @param offCommand
     */
    public void setCommand(int position, Command onCommand, Command offCommand) {
        onCommands[position] = onCommand;
        offCommands[position] = offCommand;
    }

    public void pushON(int position) {
        onCommands[position].execute();
        cancelCommand = onCommands[position];
    }

    public void pushOFF(int position) {
        offCommands[position].execute();
        cancelCommand = offCommands[position];
    }

    /**
     * 撤销上一次操作
     */
    public void cancelCommand() {
        cancelCommand.undo();
    }
}
```
#### 1.2 测试代码
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/26 14:38
 */
@Log4j2
public class RemoteControllerTest {
    @Test
    public void remoteControllerTest() {
        RemoteController remoteController = new RemoteController(3);
        Light light = new Light();
        LightOnCommand lightOnCommand = new LightOnCommand(light);
        LightOffCommand lightOffCommand = new LightOffCommand(light);
        remoteController.setCommand(0, lightOnCommand, lightOffCommand);
        TV tv = new TV();
        /*创建开电视指令*/
        TVOnCommand tvOnCommand = new TVOnCommand(tv);
        /*创建关电视指令*/
        TVOffCommand tvOffCommand = new TVOffCommand(tv);
        /*创建音量指令*/
        TVVoiceCommand tvVoiceCommand = new TVVoiceCommand(tv);
        remoteController.setCommand(1, tvOnCommand, tvOffCommand);
        remoteController.setCommand(2, tvVoiceCommand, tvVoiceCommand);
        remoteController.pushON(0);
        remoteController.pushOFF(0);
        remoteController.pushON(1);
        remoteController.pushOFF(1);
        remoteController.pushON(2);
        remoteController.pushOFF(2);
        remoteController.cancelCommand();
    }
}
```
### 小结
> 命令模式对于各种行为有一个很好的封装,也可以自己指定命令的执行顺序，如果创建了自己的命令执行数组,那么就是宏命令.
### UML类图
![命令模式](https://img-blog.csdn.net/20180426155936469?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NhRXJOdW9TaQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
源码地址 [https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/commmand](https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/commmand)