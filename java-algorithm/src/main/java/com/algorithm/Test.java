package com.algorithm;

import java.awt.*;

/**
 * @author _G5niusX
 * @date 2018/5/4
 */
public class Test {

    public static void main(String[] args) {
        MyFrame myFrame = new MyFrame();
        myFrame.init(400, 300);
    }


}

class MyFrame extends Frame {
    int width;
    int height;

    @Override
    public void paint(Graphics g) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (i % 2 == 0 || j % 2 == 0) {
                    g.setColor(Color.RED);
                } else {
                    g.setColor(Color.white);
                }
                g.fillOval(i, j, 2, 2);
            }
        }

    }

    public void init(int width, int height) {
        this.width = width;
        this.height = height;
        setSize(width, height);
        setVisible(true);
        setBackground(Color.cyan);
    }
}
