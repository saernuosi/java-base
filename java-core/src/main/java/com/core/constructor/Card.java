package com.core.constructor;

import lombok.extern.log4j.Log4j2;

/**
 * 通过tag初始化的时候的数字,判定哪一个代码先被执行
 *
 * @author SaErNuoSi
 * @date 2018/4/26 23:25
 */
@Log4j2
public class Card {
    Tag firstTag = new Tag(0);

    public Card() {
        new Tag(2);
    }

    Tag secondTag = new Tag(1);
}
