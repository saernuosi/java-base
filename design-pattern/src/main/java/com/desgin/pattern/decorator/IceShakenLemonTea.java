package com.desgin.pattern.decorator;

import java.math.BigDecimal;

/**
 * @author SaErNuoSi
 * @date 2018/4/25 13:57
 */
public class IceShakenLemonTea extends AbstractBeverage {
    public IceShakenLemonTea() {
        this.description = "冰摇柠檬茶";
    }

    @Override
    BigDecimal coast() {
        return new BigDecimal(15);
    }
}
