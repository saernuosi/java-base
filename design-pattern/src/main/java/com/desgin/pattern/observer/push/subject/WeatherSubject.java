package com.desgin.pattern.observer.push.subject;

import com.desgin.pattern.observer.WeatherData;
import com.desgin.pattern.observer.push.observer.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 天气数据类
 *
 * @Author _G5niusX
 * @Date 2018/4/24 18:42
 */
public class WeatherSubject implements Subject {

    private List<Observer> observerList;

    private WeatherData weatherData;

    public WeatherSubject() {
        this.observerList = new ArrayList<>();
    }


    @Override
    public void registerObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObserver() {
        observerList.stream().forEach(observer -> observer.update(weatherData));
    }

    /**
     * 通过该方法将天气信息通知给所有的观察者
     *
     * @param weatherData
     */
    public void publishWeather(WeatherData weatherData) {
        this.weatherData = weatherData;
        notifyObserver();
    }
}
