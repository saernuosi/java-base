package com.desgin.pattern.facade;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 13:30
 */
public class TV {
    public void open() {
        System.out.println("打开电视");
    }
}
