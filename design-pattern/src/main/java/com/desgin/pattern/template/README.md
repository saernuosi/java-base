## 模板方法模式
### 模板方法模式的概念
> 将行为模式模板化,将相同的行为抽成抽象类中的方法,不同的行为抽成抽象方法,让不同的类去实现
#### 1.1 JAVA代码
> 制作饮料的抽象类,将不同的行为做了抽象,只需要调用一个方法就可以完成饮料的制作
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/27 15:58
 */
@Log4j2
public abstract class AbstractCaffeineBeverage {
    final void prepareRecipe() {
        boilWater();
        brew();
    }

    /**
     * 做饮料的具体方法
     */
    protected abstract void brew();

    /**
     * 加调味品
     */
    protected abstract void addCondiments();

    void boilWater() {
        log.info("烧开水...");
    }
}
```
> 制作一杯咖啡,实现这个抽象的类
```java
/**
 * 制作咖啡
 *
 * @author SaErNuoSi
 * @date 2018/4/27 16:05
 */
@Log4j2
public class Coffee extends AbstractCaffeineBeverage {
    @Override
    protected void brew() {
        log.info("咖啡正在烹煮....");
    }

    @Override
    protected void addCondiments() {
        log.info("正在给咖啡加料....");
    }
}
```
#### 1.2 测试代码
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/27 16:10
 */
@Log4j2
public class TemplateTest {

    @Test
    public void templateTest() {
        AbstractCaffeineBeverage tea = new Tea();
        tea.prepareRecipe();
        tea = new Coffee();
        tea.prepareRecipe();
    }
}
```
### 小结
> 模板方法模式在真正工作中可以节省大量的业务代码,减少代码的冗余量
### UML类图
![模板方法模式](https://img-blog.csdn.net/20180427162205916?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NhRXJOdW9TaQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
源码地址 [https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/template](https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/template)