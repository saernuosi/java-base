package com.desgin.pattern.facade;

import org.junit.Test;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 13:34
 */
public class FacadeTest {

    @Test(expected = Test.None.class)
    public void facadeTest() {
        Facade facade = new Facade();
        facade.open();
    }
}
