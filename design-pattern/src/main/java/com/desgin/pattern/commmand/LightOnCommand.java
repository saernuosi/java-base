package com.desgin.pattern.commmand;

/**
 * 开灯的命令
 * @author SaErNuoSi
 * @date 2018/4/26 13:48
 */
public class LightOnCommand implements Command{
    Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.on();
    }

    @Override
    public void undo() {
        light.off();
    }
}
