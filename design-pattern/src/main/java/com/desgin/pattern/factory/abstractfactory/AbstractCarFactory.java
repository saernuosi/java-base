package com.desgin.pattern.factory.abstractfactory;

import com.desgin.pattern.factory.Car;

/**
 * 抽象工程模式,通过子类的实现来决定具体的产品
 *
 * @author SaErNuoSi
 * @date 2018/4/13 13:49
 */
public abstract class AbstractCarFactory {
    public abstract Car factoryCar();
}
