package com.desgin.pattern.chain;

import org.junit.Test;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 16:51
 */
public class ChainTest {

    @Test
    public void chainTest() {
        Customer customer = new Customer(PriceHandler.createPriceHandler());
        customer.requestDiscount(0f);
    }
}
