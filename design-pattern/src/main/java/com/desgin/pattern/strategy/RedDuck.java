package com.desgin.pattern.strategy;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 14:00
 */
public class RedDuck extends Duck {

    public RedDuck() {
        /*调用父类的set方法,实例化属于自己的飞行模式*/
        this.setFlying(() -> {
            System.out.println("红色的鸭子用想象力可以飞行...");
        });
    }

    @Override
    public void display() {
        System.out.println("红色的鸭子");
    }
}
