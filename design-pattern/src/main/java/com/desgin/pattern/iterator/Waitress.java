package com.desgin.pattern.iterator;

import lombok.extern.log4j.Log4j2;

/**
 * 女招待员,负责打印菜单等动作
 *
 * @Author _G5niusX
 * @Date 2018/5/1 17:16
 */
@Log4j2
public class Waitress {
    PancakeHouseMenu pancakeHouseMenu;
    DinerMenus dinerMenus;

    public Waitress(PancakeHouseMenu pancakeHouseMenu, DinerMenus dinerMenus) {
        this.pancakeHouseMenu = pancakeHouseMenu;
        this.dinerMenus = dinerMenus;
    }

    public void printMenus() {
        MenuIterator iterator = pancakeHouseMenu.iterator();
        print(iterator);
        MenuIterator iterator1 = dinerMenus.iterator();
        print(iterator1);

    }

    private void print(MenuIterator iterator) {
        while (iterator.hasNext()) {
            MenuItem next = (MenuItem) iterator.next();
            log.info(next.toString());
        }
    }
}
