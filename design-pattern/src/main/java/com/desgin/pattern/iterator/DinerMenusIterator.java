package com.desgin.pattern.iterator;

/**
 *
 * @Author _G5niusX
 * @Date 2018/5/1 17:22
 */
public class DinerMenusIterator implements MenuIterator {

    MenuItem[] menuItems;
    int position = 0;

    public DinerMenusIterator(MenuItem[] menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public boolean hasNext() {
        if (position >= menuItems.length || menuItems[position] == null) {
            return false;
        }
        return true;
    }

    @Override
    public Object next() {
        MenuItem menuItem = menuItems[position];
        position++;
        return menuItem;
    }
}
