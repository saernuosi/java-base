package com.desgin.pattern.factory.abstractfactory;

import com.desgin.pattern.factory.Car;
import com.desgin.pattern.factory.abstractfactory.AbstractCarFactory;

/**
 * @author SaErNuoSi
 * @date 2018/4/23 12:57
 */
public class FordCarFactory extends AbstractCarFactory {
    @Override
    public Car factoryCar() {
        return new FordCar();
    }

    class FordCar implements Car {

        @Override
        public String name() {
            return "福特";
        }

        @Override
        public int speed() {
            return 200;
        }
    }
}
