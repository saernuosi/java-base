package com.ioc;

/**
 * @Author _G5niusX
 * @Date 2018/4/17 20:48
 */
public final class BeanDefine {
    private Object bean;
    private Class<?> beanType;
    private boolean isSingle = true;

    public BeanDefine(Object bean) {
        this(bean, bean.getClass());
    }

    public BeanDefine(Object bean, Class<?> beanType) {
        this.bean = bean;
        this.beanType = beanType;
    }

    public BeanDefine(Object bean, Class<?> beanType, boolean isSingle) {
        this.bean = bean;
        this.beanType = beanType;
        this.isSingle = isSingle;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public Class<?> getBeanType() {
        return beanType;
    }

    public void setBeanType(Class<?> beanType) {
        this.beanType = beanType;
    }

    public boolean isSingle() {
        return isSingle;
    }

    public void setSingle(boolean single) {
        isSingle = single;
    }
}
