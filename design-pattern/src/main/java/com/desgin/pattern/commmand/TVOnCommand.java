package com.desgin.pattern.commmand;

/**
 * 开电视命令
 *
 * @author SaErNuoSi
 * @date 2018/4/26 14:41
 */
public class TVOnCommand implements Command {
    TV tv;

    public TVOnCommand(TV tv) {
        this.tv = tv;
    }

    @Override
    public void execute() {
        tv.on();
    }

    @Override
    public void undo() {
        tv.off();
    }
}
