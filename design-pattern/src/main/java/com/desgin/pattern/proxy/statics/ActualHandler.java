package com.desgin.pattern.proxy.statics;

import com.desgin.pattern.proxy.Handler;

/**
 * 真正的业务处理
 */
public class ActualHandler implements Handler {
    @Override
    public String doSomething(String string) {
        return "来自ActualHandler的返回:" + string.toUpperCase();
    }
}
