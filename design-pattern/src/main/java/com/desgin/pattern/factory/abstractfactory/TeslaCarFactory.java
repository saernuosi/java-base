package com.desgin.pattern.factory.abstractfactory;

import com.desgin.pattern.factory.Car;

/**
 * @author SaErNuoSi
 * @date 2018/4/23 13:01
 */
public class TeslaCarFactory extends AbstractCarFactory {
    @Override
    public Car factoryCar() {
        return new TeslaCar();
    }

    class TeslaCar implements Car {

        @Override
        public String name() {
            return "特斯拉";
        }

        @Override
        public int speed() {
            return 250;
        }
    }
}
