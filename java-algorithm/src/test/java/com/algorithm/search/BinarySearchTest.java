package com.algorithm.search;

import org.junit.Assert;
import org.junit.Test;

/**
 * @Author _G5niusX
 * @Date 2018/4/27 11:56
 */
public class BinarySearchTest {
    @Test
    public void binarySearchTest() {
        int[] ints = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        int key = 2;
        int rank = BinarySearch.rank(key, ints);
        if (rank != -1) {
            Assert.assertEquals(ints[rank], key);
        }
    }
}
