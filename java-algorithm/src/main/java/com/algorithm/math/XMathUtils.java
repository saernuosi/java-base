package com.algorithm.math;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * @Author _G5niusX
 * @Date 2018/4/27 11:44
 */
@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class XMathUtils {

    /**
     * 计算一个整数的绝对值
     *
     * @param num
     * @return
     */
    public static int abs(int num) {
        return num < 0 ? (num == Integer.MIN_VALUE ? Integer.MAX_VALUE : -num) : num;
    }

}
