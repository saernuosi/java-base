package com.desgin.pattern.strategy;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 14:07
 */
@Log4j2
public class StrategyTest {

    @Test
    public void strategyTest() {
        Duck redDuck = new RedDuck();
        redDuck.display();
        redDuck.duck();
        redDuck.fly();
        Duck yellowDuck = new YellowDuck();
        yellowDuck.display();
        yellowDuck.duck();
        yellowDuck.fly();
    }

    @Test
    public void normalCalcTest() {
        String type = "A";
        Assert.assertEquals(normalCalc(type), strategyCalc(type));
    }

    /**
     * 传入一个类型,正常人的写法，如果在每个if判断里面有很多的业务处理就需要引入策略模式来实现，虽然没什么卵用
     *
     * @param type
     * @return
     */
    public String normalCalc(String type) {
        if ("A".equals(type)) {
            /*在这做了N多行的业务处理*/
            return "A执行完毕";
        }
        if ("B".equals(type)) {
            /*在这做了N多行的业务处理*/
            return "B执行完毕";
        }
        if ("C".equals(type)) {
            /*在这做了N多行的业务处理*/
            return "C执行完毕";
        }
        if ("D".equals(type)) {
            /*在这做了N多行的业务处理*/
            return "D执行完毕";
        }
        return "不支持类型";
    }

    /**
     * 使用策略模式的写法
     *
     * @param type
     * @return
     */
    public String strategyCalc(String type) {
        return StrategyFactory.instance().calc(type);
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    static class StrategyFactory {
        public static final HashMap<String, CalcStrategy> HASH_MAP = new HashMap<>();


        static {
            /*初始化每个类型的子类,然后各自实现N多行的业务处理*/
            CalcStrategy aCalcStrategy = () -> "A执行完毕";
            CalcStrategy bCalcStrategy = () -> "B执行完毕";
            CalcStrategy cCalcStrategy = () -> "C执行完毕";
            CalcStrategy dCalcStrategy = () -> "D执行完毕";
            HASH_MAP.put("A", aCalcStrategy);
            HASH_MAP.put("B", bCalcStrategy);
            HASH_MAP.put("C", cCalcStrategy);
            HASH_MAP.put("D", dCalcStrategy);
        }

        public String calc(String type) {
            return HASH_MAP.get(type) == null ? "不支持类型" : HASH_MAP.get(type).calc();
        }

        public static StrategyFactory instance() {
            return new StrategyFactory();
        }
    }
}
