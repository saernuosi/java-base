package com.desgin.pattern.commmand;

/**
 * 关灯命令
 * @author SaErNuoSi
 * @date 2018/4/26 15:05
 */
public class LightOffCommand implements Command {
    Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.off();
    }

    @Override
    public void undo() {
        light.on();
    }
}
