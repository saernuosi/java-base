package com.desgin.pattern.template;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;

/**
 * @author SaErNuoSi
 * @date 2018/4/27 16:10
 */
@Log4j2
public class TemplateTest {

    @Test
    public void templateTest() {
        AbstractCaffeineBeverage tea = new Tea();
        tea.prepareRecipe();
        tea = new Coffee();
        tea.prepareRecipe();
    }
}
