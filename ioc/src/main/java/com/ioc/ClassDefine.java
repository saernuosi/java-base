package com.ioc;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author SaErNuoSi
 * @date 2018/4/18 10:51
 */
public final class ClassDefine {
    private final Class<?> clazz;
    private static final Map<Class<?>, ClassDefine> MAP = new ConcurrentHashMap<>(32);

    public ClassDefine(Class<?> clazz) {
        this.clazz = clazz;
    }

    public static ClassDefine create(Class<?> clazz) {
        ClassDefine classDefine = MAP.get(clazz);
        if (classDefine == null) {
            ClassDefine classDefine1 = new ClassDefine(clazz);
            return MAP.putIfAbsent(clazz, classDefine1);
        }
        return classDefine;
    }

    public Class<?> getClazz() {
        return clazz;
    }
}
