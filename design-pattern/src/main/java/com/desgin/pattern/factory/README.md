## 工厂模式
### 工厂模式的概念
> 顾名思义就是像工厂一样生产产品(类实例)的一种模式.
### JAVA中的工厂模式
> 工厂模式分为简单工厂和抽象工厂两种
#### 1 简单工厂
#### 1.1 JAVA代码
> 简单工厂模式,通过判断来产生各种产品
```java
/**
 * 简单工厂模式,通过判断来产生各种产品
 *
 * @author SaErNuoSi
 * @date 2018/4/23 12:38
 */
public class SimpleCarFactory {
    public static Car factory(String type) {
        switch (type) {
            case "SUV":
                return new SUVCar();
            case "SPECIAL":
                return new SpecialCar();
            default:
                return new Car() {
                    @Override
                    public String name() {
                        return "大众";
                    }

                    @Override
                    public int speed() {
                        return 200;
                    }
                };
        }
    }
}
```
##### 1.2 测试代码
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/13 14:26
 */
public class FactoryTest {
    
    @Test
    public void simpleFactoryTest() {
        Car bmwCar = SimpleCarFactory.factory("BMW");
        Assert.assertNotNull(bmwCar);
    }
}
```
#### 2 抽象工厂模式
> 抽象工厂模式是用来生产一些抽象的产品,具体的生产信息会让它的子类去实现
#### 2.1 JAVA代码
```java
/**
 * 抽象工程模式,通过子类的实现来决定具体的产品
 *
 * @author SaErNuoSi
 * @date 2018/4/13 13:49
 */
public abstract class AbstractCarFactory {
    public abstract Car factoryCar();
}
```
> 通过继承上面的抽象方法来生产自己的汽车
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/23 13:01
 */
public class TeslaCarFactory extends AbstractCarFactory {
    @Override
    public Car factoryCar() {
        return new TeslaCar();
    }

    class TeslaCar implements Car {

        @Override
        public String name() {
            return "特斯拉";
        }

        @Override
        public int speed() {
            return 250;
        }
    }
}
```
> 上面的代码会生产出特斯拉汽车
#### 2.2 测试代码
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/13 14:26
 */
@Log4j2
public class FactoryTest {

    @Test
    public void abstractFactoryTest() {
        TeslaCarFactory teslaCarFactory = new TeslaCarFactory();
        Car car = teslaCarFactory.factoryCar();
        Assert.assertNotNull(car);
        log.info(car.detail());
    }
}
```
### 工厂模式小结
> 工厂模式的主要用途就是来生产多个类,是我们经常使用的一种设计模式

源码地址 [https://gitee.com/saernuosi/java-base](https://gitee.com/saernuosi/java-base)