package com.core.constructor;

import lombok.extern.log4j.Log4j2;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 23:24
 */
@Log4j2
public class Tag {
    public Tag(int i) {
        log.info("标记的数字: {}", i);
    }
}
