package com.desgin.pattern.proxy.jdk;

import com.desgin.pattern.proxy.Handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * JDK动态代理
 */
public class JDKProxyHandler implements InvocationHandler {

    private Handler handler;

    public JDKProxyHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(handler, args);
    }
}
