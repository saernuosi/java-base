package com.ioc;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * @author SaErNuoSi
 * @date 2018/4/19 13:17
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Log4j2
public class ReflectiveUtils {

    public static <T> T getInstance(Class<T> tClass) {
        try {
            return tClass.newInstance();
        } catch (InstantiationException e) {
            log.error("实例化bean:", e, e.getMessage());
        } catch (IllegalAccessException e) {
            log.error("实例化bean:", e, e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        String instance = getInstance(String.class);
        System.out.println(instance);
    }
}
