package com.core;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 20:28
 */
public class Application {
    static {
        System.out.println("static");
    }

    {
        int a = 1;
        System.out.println(a);
    }

    {
        System.out.println("2");
    }

    public static void main(String[] args) {
        System.out.println("main");
    }
}
