package com.desgin.pattern.singleton;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.stream.Stream;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 18:26
 */
@Log4j2
public class HungerSingletonTest {
    @Test
    public void hungerSingletonTest() {
        for (int i = 0; i < 200; i++) {
            new Thread(() -> log.info(HungerSingleton.instance())).start();
        }
    }

    /**
     * 通过反射可以使饿汉的单例失效
     */
    @Test
    public void invokeInstanceTest() {

        /*因为设置了构造方法修饰符为private，直接反射报IllegalAccessException*/
        Stream.of(HungerSingleton.class.getDeclaredConstructors()).forEach(constructor -> {
            constructor.setAccessible(true);
            for (int i = 0; i < 5; i++) {
                Object instance = null;
                try {
                    instance = constructor.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                /*通过日志可以发现内存地址发生了改变*/
                log.info(instance);
            }
        });

    }
}
