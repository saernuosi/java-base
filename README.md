# _G5niusX的JAVA基础
> [CSDN博客](https://blog.csdn.net/SaErNuoSi)
-----
* [JAVA核心](https://gitee.com/saernuosi/java-base/blob/master/java-core)
* [设计模式](https://gitee.com/saernuosi/java-base/blob/master/design-pattern)
* [简单的IOC](https://gitee.com/saernuosi/java-base/blob/master/ioc)
* [常见的数据结构](https://gitee.com/saernuosi/java-base/tree/master/java-algorithm)
* [kotlin相关(未开始)](https://gitee.com/saernuosi/java-base/tree/master/kotlin)
-----
![404](https://img-blog.csdn.net/20180426174104736?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NhRXJOdW9TaQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)