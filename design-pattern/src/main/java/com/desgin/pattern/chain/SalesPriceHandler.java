package com.desgin.pattern.chain;

/**
 * 销售人员可以批准的折扣率
 *
 * @Author _G5niusX
 * @Date 2018/4/22 16:40
 */
public class SalesPriceHandler extends PriceHandler {


    public SalesPriceHandler(String name) {
        super(name);
    }

    @Override
    void processPrice(float discount) {
        if (discount <= 0.05) {
            System.out.format("%s批准了%.2f折扣%n", name, discount);
        } else {
            nextPriceHandler.processPrice(discount);
        }
    }


}
