package com.desgin.pattern.adapter;

public class ChinaPower extends AmericanPower implements CPower {
    public void chinaCharge() {
        charge();
    }
}

interface CPower {
    void chinaCharge();
}
