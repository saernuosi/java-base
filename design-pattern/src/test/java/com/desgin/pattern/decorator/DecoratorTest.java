package com.desgin.pattern.decorator;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;

/**
 * @author SaErNuoSi
 * @date 2018/4/19 17:22
 */
@Log4j2
public class DecoratorTest {

    @Test
    public void decoratorTest() {
        /*先生成冰摇柠檬茶*/
        IceShakenLemonTea iceShakenLemonTea = new IceShakenLemonTea();
        /*加摩卡*/
        Mocha mocha = new Mocha(iceShakenLemonTea);
        /*加奶泡*/
        Whip whip = new Whip(mocha);
        log.info("购买一杯:" + whip.getDescription() + "花费:" + whip.coast() + "$");
    }
}
