package com.desgin.pattern.command;

import com.desgin.pattern.commmand.*;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 14:38
 */
@Log4j2
public class RemoteControllerTest {
    @Test
    public void remoteControllerTest() {
        RemoteController remoteController = new RemoteController(3);
        Light light = new Light();
        LightOnCommand lightOnCommand = new LightOnCommand(light);
        LightOffCommand lightOffCommand = new LightOffCommand(light);
        remoteController.setCommand(0, lightOnCommand, lightOffCommand);
        TV tv = new TV();
        /*创建开电视指令*/
        TVOnCommand tvOnCommand = new TVOnCommand(tv);
        /*创建关电视指令*/
        TVOffCommand tvOffCommand = new TVOffCommand(tv);
        /*创建音量指令*/
        TVVoiceCommand tvVoiceCommand = new TVVoiceCommand(tv);
        remoteController.setCommand(1, tvOnCommand, tvOffCommand);
        remoteController.setCommand(2, tvVoiceCommand, tvVoiceCommand);
        remoteController.pushON(0);
        remoteController.pushOFF(0);
        remoteController.pushON(1);
        remoteController.pushOFF(1);
        remoteController.pushON(2);
        remoteController.pushOFF(2);
        remoteController.cancelCommand();
    }
}
