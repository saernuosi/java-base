package com.desgin.pattern.observer;

/**
 * @Author _G5niusX
 * @Date 2018/4/24 19:02
 */
public interface DisplayElement {
    /**
     * 天气显示器
     */
    void display();
}
