package com.desgin.pattern.observer.pull;

/**
 * @author SaErNuoSi
 * @date 2018/4/25 8:56
 */
@FunctionalInterface
public interface PullObserver {
    /**
     * 拉模型的重点,将整个对象都传递给观察者，让观察者来选择自己的数据
     *
     * @param weatherObservable
     * @param object
     */
    void update(WeatherObservable weatherObservable, Object object);
}
