package com.desgin.pattern.factory;

import com.desgin.pattern.factory.abstractfactory.TeslaCarFactory;
import com.desgin.pattern.factory.simple.SimpleCarFactory;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author SaErNuoSi
 * @date 2018/4/13 14:26
 */
@Log4j2
public class FactoryTest {

    @Test
    public void abstractFactoryTest() {
        TeslaCarFactory teslaCarFactory = new TeslaCarFactory();
        Car car = teslaCarFactory.factoryCar();
        Assert.assertNotNull(car);
        log.info(car.detail());
    }

    @Test
    public void simpleFactoryTest() {
        Car bmwCar = SimpleCarFactory.factory("BMW");
        Assert.assertNotNull(bmwCar);
        log.info(bmwCar.detail());
    }
}
