package com.desgin.pattern.observer.push.observer;

import com.desgin.pattern.observer.WeatherData;

/**
 * 观察者接口,它的实现类可以被加入到主题中
 *
 * @Author _G5niusX
 * @Date 2018/4/24 18:54
 */
public interface Observer {
    /**
     * 天气的主题会调用这个方法将温度,湿度,压力通知给所有的观察者
     *
     * @param weatherData
     */
    void update(WeatherData weatherData);
}
