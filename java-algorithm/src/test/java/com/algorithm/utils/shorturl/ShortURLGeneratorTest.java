package com.algorithm.utils.shorturl;

import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author _G5niusX
 * @Date 2018/4/24 21:27
 */
@Log4j2
public class ShortURLGeneratorTest {


    @Test
    public void shortURLGeneratorTest() {
        Set<String> stringSet = new HashSet<>();
        int length = 10 << 10 << 8;
        for (int i = 0; i < length; i++) {
            String system62System = ShortURLGenerator.decimalSystem62System(i);
            stringSet.add(system62System);
        }
        Assert.assertEquals(stringSet.size(), length);
    }

}
