## 策略模式
### 策略模式的概念
> 策略模式是指对一系列的算法定义，并将每一个算法封装起来，而且使它们还可以相互替换。策略模式让算法独立于使用它的客户而独立变化
> 策略模式的优点是提供了管理相关的算法族的办法、策略模式提供了可以替换继承关系的办法、使用策略模式可以避免使用多重条件转移语句
### JAVA中的策略模式
#### 1 java代码
> 1.1 建立一个抽象类,抽象方法委托给子类,让子类去实现,同时抽象类申明一个Flying接口.提供set方法给子类去实例化这个变量
```java
/**
 * @Author _G5niusX
 * @Date 2018/4/22 13:46
 */
public abstract class Duck {
    private Flying flying;

    /**
     * 鸭子的形状
     */
    public abstract void display();

    public void duck() {
        System.out.println("鸭子叫....");
    }

    public void setFlying(Flying flying) {
        this.flying = flying;
    }

    public void fly() {
        this.flying.fly();
    }
}
```
> 小黄鸭,属于鸭子的一种但是不会飞行
```java
/**
 * 玩具小黄鸭,不会飞行
 *
 * @author SaErNuoSi
 * @date 2018/4/24 9:58
 */
public class YellowDuck extends Duck {
    public YellowDuck() {
        this.setFlying(() -> {
            System.out.println("我不会飞行...");
        });
    }

    @Override
    public void display() {
        System.out.println("小黄鸭...一个小玩具");
    }
}
```
> 红色的鸭子
```java
/**
 * @Author _G5niusX
 * @Date 2018/4/22 14:00
 */
public class RedDuck extends Duck {

    public RedDuck() {
        /*调用父类的set方法,实例化属于自己的飞行模式*/
        this.setFlying(() -> {
            System.out.println("红色的鸭子用想象力可以飞行...");
        });
    }

    @Override
    public void display() {
        System.out.println("红色的鸭子");
    }
}

```
#### 2 测试代码
```java
/**
 * @Author _G5niusX
 * @Date 2018/4/22 14:07
 */
@Log4j2
public class StrategyTest {

    @Test
    public void strategyTest() {
        Duck redDuck = new RedDuck();
        redDuck.display();
        redDuck.duck();
        redDuck.fly();
        Duck yellowDuck = new YellowDuck();
        yellowDuck.display();
        yellowDuck.duck();
        yellowDuck.fly();
    }
}
```
### 类图
![策略模式类图](https://img-blog.csdn.net/20180424110717198?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NhRXJOdW9TaQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
### 策略模式小结
> 策略模式通过在抽象类持有一个接口变量,然后把接口变量的具体实现分配给每个不同的子类,实现了不同的策略

源码地址 [https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/strategy](https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/strategy)