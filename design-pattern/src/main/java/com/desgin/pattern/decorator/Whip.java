package com.desgin.pattern.decorator;

import java.math.BigDecimal;

/**
 * 奶泡
 *
 * @author SaErNuoSi
 * @date 2018/4/25 14:11
 */
public class Whip extends CondimentDecorator {

    AbstractBeverage abstractBeverage;

    public Whip(AbstractBeverage abstractBeverage) {
        this.abstractBeverage = abstractBeverage;
    }

    @Override
    public String getDescription() {
        return "[奶泡]" + abstractBeverage.getDescription();
    }

    @Override
    BigDecimal coast() {
        return new BigDecimal(5).add(abstractBeverage.coast());
    }
}
