package com.algorithm.tree;

import com.algorithm.tree.BTree;
import com.algorithm.tree.TreeNode;
import org.junit.Before;
import org.junit.Test;

/**
 * 二叉树测试
 *
 * @author SaErNuoSi
 * @date 2018/4/11 9:30
 */
public class TreeTest {

    private TreeNode root;

    @Before
    public void init() {
        /*先创建一个根节点*/
        root = new TreeNode(10);
        /*创建剩余的四个节点*/
        TreeNode secondLeftTreeNode = new TreeNode(9);
        TreeNode secondRightTreeNode = new TreeNode(20);
        TreeNode threeLeftTreeNode = new TreeNode(15);
        TreeNode threeRightTreeNode = new TreeNode(35);
        /*给根节点设置左节点和右节点*/
        root.setLeftTreeNode(secondLeftTreeNode);
        root.setRightTreeNode(secondRightTreeNode);
        /*给第二层的右节点设置左叶子和右叶子*/
        secondRightTreeNode.setLeftTreeNode(threeLeftTreeNode);
        secondRightTreeNode.setRightTreeNode(threeRightTreeNode);
    }

    @Test
    public void baseTreeTest() {

        /*遍历这个二叉树*/
        /*1.中序遍历,根节点->左节点->右节点*/
        /*2.先序遍历,左节点->根节点->右节点*/
        /*3.后序遍历,左节点->右节点->根节点*/
        /*以上的3种遍历,如果有任何一个节点有子节点,则会有限处理当前的子节点*/
        System.out.println("------- 中序遍历 -------");
        inTraverseBTree(root);
        System.out.println("------- 先序遍历 -------");
        preTraverseTreeNode(root);
        System.out.println("------- 后序遍历 -------");
        afterTraverseTreeNode(root);
    }

    @Test
    public void bTreeTest() {
        int[] ints = {2, 3, 1, 4, 5};
        BTree bTree = new BTree();
        for (int i : ints) {
            createBTree(bTree, i);
        }
        preTraverseTreeNode(bTree.getTreeNode());
        System.out.println("------ 分割线 ------");
        inTraverseBTree(bTree.getTreeNode());
    }

    @Test
    public void treeHeightTest() {
        int height = getHeight(root);
        System.out.println(height);
    }

    public int getHeight(TreeNode treeNode) {
        if (treeNode == null) {
            return 0;
        }
        int leftHeight = getHeight(treeNode.getLeftTreeNode());
        int rightHeight = getHeight(treeNode.getRightTreeNode());
        int max = leftHeight;
        /*如果右边的高度大于左边的高度*/
        if (rightHeight > max) {
            max = rightHeight;
        }
        return max + 1;
    }

    /**
     * 动态创建Btree
     *
     * @param bTree
     * @param value
     */
    public void createBTree(BTree bTree, int value) {
        /*如果BTreee没有子节点*/
        if (bTree.getTreeNode() == null) {
            TreeNode treeNode = new TreeNode(value);
            bTree.setTreeNode(treeNode);
        } else {
            TreeNode treeNode = bTree.getTreeNode();
            while (treeNode != null) {
                /*如果当前值大于了根节点的值,需要去右节点*/
                if (value > treeNode.getValue()) {
                    /*如果当前根节点没有右节点。则创建一个右节点*/
                    if (treeNode.getRightTreeNode() == null) {
                        treeNode.setRightTreeNode(new TreeNode(value));
                        return;
                    } else {
                        treeNode = treeNode.getRightTreeNode();
                    }
                } else {
                    if (treeNode.getLeftTreeNode() == null) {
                        treeNode.setLeftTreeNode(new TreeNode(value));
                        return;
                    } else {
                        treeNode = treeNode.getLeftTreeNode();
                    }
                }
            }
        }
    }

    /**
     * 中序遍历
     * 根节点->左节点->右节点
     *
     * @param treeNode
     */
    private void inTraverseBTree(TreeNode treeNode) {
        if (treeNode != null) {
            System.out.println(treeNode.getValue());
            inTraverseBTree(treeNode.getLeftTreeNode());
            inTraverseBTree(treeNode.getRightTreeNode());
        }
    }

    /**
     * 先序遍历
     * ,左节点->根节点->右节点
     *
     * @param treeNode
     * @return
     */
    private String preTraverseTreeNode(TreeNode treeNode) {
        if (treeNode != null) {
            preTraverseTreeNode(treeNode.getLeftTreeNode());
            System.out.println(treeNode.getValue());
            preTraverseTreeNode(treeNode.getRightTreeNode());
        }
        return null;
    }

    /**
     * 后序遍历
     *
     * @param treeNode
     * @return
     */
    private String afterTraverseTreeNode(TreeNode treeNode) {
        if (treeNode != null) {
            afterTraverseTreeNode(treeNode.getLeftTreeNode());
            afterTraverseTreeNode(treeNode.getRightTreeNode());
            System.out.println(treeNode.getValue());
        }
        return null;
    }

}
