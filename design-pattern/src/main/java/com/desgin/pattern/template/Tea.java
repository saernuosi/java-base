package com.desgin.pattern.template;

import lombok.extern.log4j.Log4j2;

/**
 * @author SaErNuoSi
 * @date 2018/4/27 16:07
 */
@Log4j2
public class Tea extends AbstractCaffeineBeverage {
    @Override
    protected void brew() {
        log.info("茶正在烹煮...");
    }

    @Override
    protected void addCondiments() {
        log.info("正在给茶加料...");
    }
}
