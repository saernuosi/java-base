package com.desgin.pattern.proxy;

@FunctionalInterface
public interface Handler {
    String doSomething(String string);
}
