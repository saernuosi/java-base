package com.desgin.pattern.adapter;

import org.junit.Test;

public class AdapterTest {
    @Test
    public void classAdapter() {
        ChinaPower chinaPower = new ChinaPower();
        chinaPower.charge();
        chinaPower.chinaCharge();
    }
}
