## 迭代器模式
### 迭代器模式的概念
> 根据顺序访问集合对象的元素,并不需要知道集合对象的底层表示和实现,在JAVA中有者广泛的使用,尤其是在集合里面.
#### 1.1 JAVA代码
> 本例子会使用迭代器模式来实现2个类的遍历,一个类中的集合是数组,一个类中的集合是LIST

-----
> 先创建一个迭代器接口
```java
/**
 * 菜单迭代器
 *
 * @Author _G5niusX
 * @Date 2018/5/1 17:21
 */
public interface MenuIterator {
    boolean hasNext();

    Object next();
}
```
> 对于不同的类,实现不同的迭代器
```java
/**
 *
 * @Author _G5niusX
 * @Date 2018/5/1 17:22
 */
public class DinerMenusIterator implements MenuIterator {

    MenuItem[] menuItems;
    int position = 0;

    public DinerMenusIterator(MenuItem[] menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public boolean hasNext() {
        if (position >= menuItems.length || menuItems[position] == null) {
            return false;
        }
        return true;
    }

    @Override
    public Object next() {
        MenuItem menuItem = menuItems[position];
        position++;
        return menuItem;
    }
}
```
#### 1.2 测试代码
```java
/**
 * @Author _G5niusX
 * @Date 2018/5/1 17:34
 */
public class IteratorTest {

    @Test
    public void complexTest() {
        PancakeHouseMenu pancakeHouseMenu = new PancakeHouseMenu();
        DinerMenus dinerMenus = new DinerMenus();
        Waitress waitress = new Waitress(pancakeHouseMenu, dinerMenus);
        waitress.printMenus();
    }
}
```
> 测试结果
![迭代器模式测试结果](https://img-blog.csdn.net/2018050117454033?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NhRXJOdW9TaQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
### 小结
> 迭代器模式是一种应用很广泛的模式,在LIST接口和MAP接口中也使用了迭代器来遍历集合中的元素,在实现了迭代器接口以后就不需要再去关心内部的数据结构.达到了解耦合的效果.
### UML类图
![UML类图](https://img-blog.csdn.net/20180501174744969?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NhRXJOdW9TaQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

---------

源码地址 [https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/iterator](https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/iterator)