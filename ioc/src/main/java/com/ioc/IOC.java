package com.ioc;

/**
 * @Author _G5niusX
 * @Date 2018/4/17 20:45
 */
public interface IOC {

    void addBean(Object object);

    void addBean(String name, Object object);

    void addBean(String name, BeanDefine beanDefine);

    Object getBean(String name);

    <T> T getBean(Class<T> tClass);
}
