package com.ioc;

import java.lang.reflect.Field;

/**
 * @author SaErNuoSi
 * @date 2018/4/18 9:34
 */
@FunctionalInterface
public interface InjectInterface {
    void inject(Object object);
}
