package com.algorithm.node;

/**
 * 单向链表
 *
 * @Author _G5niusX
 * @Date 2018/4/21 4:40
 */
public class Node<T> {

    private T data;
    private Node nextNode;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Node getNextNode() {
        return nextNode;
    }

    public void setNextNode(Node nextNode) {
        this.nextNode = nextNode;
    }

    public Node(T data) {
        this.data = data;
    }

    public Node(T data, Node nextNode) {
        this.data = data;
        this.nextNode = nextNode;
    }

    public Node() {
    }

    public void addData(T data) {
        Node<T> node = new Node<>(data);
        Node<T> temp = this;
        while (temp.getNextNode() != null) {
            temp = temp.getNextNode();
        }
        temp.nextNode = node;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", nextNode=" + nextNode +
                '}';
    }
}
