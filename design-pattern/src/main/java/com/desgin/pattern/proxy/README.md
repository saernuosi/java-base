## 代理模式
### 代理模式的概念
> 为其他对象提供一种代理以控制对这个对象的访问。在某些情况下，
> 一个对象不适合或者不能直接引用另一个对象，而代理对象可以在客户端和目标对象之间起到中介的作用。
### JAVA中的代理模式
#### 1 静态代理(接口代理)
> 静态代理是通过代理类和真正的业务类实现同一个接口,然后代理类持有这个接口的变量来完成代理。

##### 1.1 java代码
```java
@FunctionalInterface
public interface Handler {
    String doSomething(String string);
}
```
```java
/**
 * 静态代理模式
 */
public class ProxyHandler implements Handler {

    private Handler handler;

    public ProxyHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public String doSomething(String string) {
        return handler.doSomething(string);
    }
}
```
```java
/**
 * 真正的业务处理
 */
public class ActualHandler implements Handler {
    @Override
    public String doSomething(String string) {
        return "来自ActualHandler的返回:" + string.toUpperCase();
    }
}
```
##### 1.2 测试代码
```java
/**
 * 代理模式测试类
 */
public class ProxyTest {
    @Test
    public void proxyTest() {
        ActualHandler actualHandler = new ActualHandler();
        ProxyHandler proxyHandler = new ProxyHandler(actualHandler);
        String test = proxyHandler.doSomething("test");
        System.out.println(test);
    }

    @Test
    public void proxyUserLambdaTest() {
        ProxyHandler proxyHandler = new ProxyHandler(string -> "lambda测试:" + string.toUpperCase());
        String s = proxyHandler.doSomething("ee");
        System.out.println(s);
    }
}
```
#### 2 动态代理
> 动态代理一般是实现JDK的InvocationHandler的接口,在实现类中持有需要代理的类,然后通过反射去调用
##### 2.1 java代码
```java
/**
 * JDK动态代理
 */
public class JDKProxyHandler implements InvocationHandler {

    private Handler handler;

    public JDKProxyHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(handler, args);
    }
}
```
##### 2.1 测试代码
```java
/**
 * 代理模式测试类
 */
public class ProxyTest {
    @Test
    public void jdbProxyTest() {
        Handler handler = string -> "JDK动态代理:" + string.toUpperCase();
        JDKProxyHandler jdkProxyHandler = new JDKProxyHandler(handler);
        Handler hasProxyHandler = (Handler) Proxy.newProxyInstance(handler.getClass().getClassLoader(), handler.getClass().getInterfaces(), jdkProxyHandler);
        String string = hasProxyHandler.doSomething("qwer");
        System.out.println(string);
    }
}
```
### 代理模式小结
> 代理模式可以通过持有真实业务类,通过直接(接口代理)或间接(动态代理)的方法去调用真实业务类的方法,对于调用者来讲,只有代理类,并不可见代理类后面的业务处理类.

源码地址 [https://gitee.com/saernuosi/java-base](https://gitee.com/saernuosi/java-base)