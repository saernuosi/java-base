package com.desgin.pattern.strategy;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 14:30
 */
@FunctionalInterface
public interface CalcStrategy {
    String calc();
}
