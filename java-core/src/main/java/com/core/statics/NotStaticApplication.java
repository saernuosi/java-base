package com.core.statics;

import lombok.extern.log4j.Log4j2;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 22:08
 */
@Log4j2
public class NotStaticApplication {
    private int i = 90;

    public NotStaticApplication() {
        i++;
        log.info("没有被static修饰的i的值" + i);
    }
}
