package com.desgin.pattern.decorator;

import java.math.BigDecimal;

/**
 * 饮料基类
 *
 * @author SaErNuoSi
 * @date 2018/4/25 13:05
 */
public abstract class AbstractBeverage {

    protected String description;

    public String getDescription() {
        return description;
    }

    /**
     * 计算饮料的价格
     *
     * @return
     */
    abstract BigDecimal coast();
}
