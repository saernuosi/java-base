package com.core.statics;

import lombok.extern.log4j.Log4j2;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 21:25
 */
@Log4j2
public class StaticApplication {
    public static int i = 90;

    public StaticApplication() {
        i++;
        log.info(this + "中i的值:" + i);
    }


}
