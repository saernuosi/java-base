package com.desgin.pattern.decorator;

/**
 * 调料的抽象类
 *
 * @author SaErNuoSi
 * @date 2018/4/25 13:50
 */
public abstract class CondimentDecorator extends AbstractBeverage {
    public abstract String getDescription();
}
