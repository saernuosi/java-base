package com.desgin.pattern.adapter;

public class AmericanPower implements APower {
    public void charge() {
        System.out.println("美式充电器....");
    }
}

interface APower {
    void charge();
}
