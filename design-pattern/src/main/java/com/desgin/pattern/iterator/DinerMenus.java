package com.desgin.pattern.iterator;

/**
 * @Author _G5niusX
 * @Date 2018/5/1 17:11
 */
public class DinerMenus {
    static final int MAX_LENGTH = 5;
    int numberOfItems = 0;
    MenuItem[] menuItems;

    public DinerMenus() {
        menuItems = new MenuItem[MAX_LENGTH];
        addItem("烤全羊", "内蒙古烤全羊", 300);
        addItem("烤鱼", "巫山烤鱼", 150);
    }

    public MenuIterator iterator() {
        return new DinerMenusIterator(menuItems);
    }

    private void addItem(String name, String description, double price) {
        if (numberOfItems <= MAX_LENGTH) {
            MenuItem menuItem = new MenuItem(name, description, price);
            menuItems[numberOfItems] = menuItem;
            numberOfItems++;
        }
    }

    public MenuItem[] getMenuItems() {
        return menuItems;
    }
}
