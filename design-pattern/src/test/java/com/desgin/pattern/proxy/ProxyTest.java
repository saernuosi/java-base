package com.desgin.pattern.proxy;

import com.desgin.pattern.proxy.jdk.JDKProxyHandler;
import com.desgin.pattern.proxy.statics.ActualHandler;
import com.desgin.pattern.proxy.statics.ProxyHandler;
import org.junit.Test;

import java.lang.reflect.Proxy;

/**
 * 代理模式测试类
 */
public class ProxyTest {
    @Test
    public void proxyTest() {
        ActualHandler actualHandler = new ActualHandler();
        ProxyHandler proxyHandler = new ProxyHandler(actualHandler);
        String test = proxyHandler.doSomething("test");
        System.out.println(test);
    }

    @Test
    public void proxyUserLambdaTest() {
        ProxyHandler proxyHandler = new ProxyHandler(string -> "lambda测试:" + string.toUpperCase());
        String s = proxyHandler.doSomething("ee");
        System.out.println(s);
    }

    @Test
    public void jdbProxyTest() {
        Handler handler = string -> "JDK动态代理:" + string.toUpperCase();
        JDKProxyHandler jdkProxyHandler = new JDKProxyHandler(handler);
        Handler hasProxyHandler = (Handler) Proxy.newProxyInstance(handler.getClass().getClassLoader(), handler.getClass().getInterfaces(), jdkProxyHandler);
        String string = hasProxyHandler.doSomething("qwer");
        System.out.println(string);
    }
}
