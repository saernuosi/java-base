package com.desgin.pattern.observer;

import lombok.Getter;
import lombok.Setter;

/**
 * 封装天气数据
 *
 * @Author _G5niusX
 * @Date 2018/4/24 19:10
 */
@Getter
@Setter
public class WeatherData {
    /**
     * 温度
     */
    private float temperature;
    /**
     * 湿度
     */
    private float humidity;
    /**
     * 压力
     */
    private float pressure;

}
