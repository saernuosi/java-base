package com.desgin.pattern.facade;

/**
 * 不常用的一种设计模式..感觉很鸡肋
 *
 * @Author _G5niusX
 * @Date 2018/4/22 13:32
 */
public class Facade {

    private Light light;
    private TV tv;
    private Video video;

    public Facade() {
        this.light = new Light();
        this.tv = new TV();
        this.video = new Video();
    }

    public void open() {
        light.open();
        tv.open();
        video.open();
    }
}
