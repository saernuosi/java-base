package com.desgin.pattern.commmand;

/**
 * 关电视命令
 *
 * @author SaErNuoSi
 * @date 2018/4/26 15:05
 */
public class TVOffCommand implements Command {
    TV tv;
    /**
     * 上一次的音量
     */
    int preVoice;

    public TVOffCommand(TV tv) {
        this.tv = tv;
        preVoice = 30;
    }

    @Override
    public void execute() {
        tv.off();
    }

    @Override
    public void undo() {
        tv.on();
    }
}
