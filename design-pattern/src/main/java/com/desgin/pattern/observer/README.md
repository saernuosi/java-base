## 观察者模式
### 观察者模式的概念
> 观察者模式定义了对象之间的一对多依赖关系,当一个对象改变状态的时候,所有的依赖者都能收到通知.
### JAVA中的观察者模式
> 观察者有两种实现方法,一种是推模型,还有一种是拉模型
#### 1 推模型
> 由订阅者主动把消息推(push)给订阅者
##### 1.1 java代码
> 订阅者接口
```java
/**
 * 主题接口,它的实现类会通知它的订阅者
 *
 * @Author _G5niusX
 * @Date 2018/4/24 18:52
 */
public interface Subject {
    /**
     * 注册订阅者
     */
    void registerObserver(Observer observer);

    /**
     * 移除订阅者
     */
    void removeObserver(Observer observer);

    /**
     * 通知订阅者
     */
    void notifyObserver();
}
```
> 具体的天气订阅者
```java
/**
 * 天气数据类
 *
 * @Author _G5niusX
 * @Date 2018/4/24 18:42
 */
public class WeatherSubject implements Subject {

    private List<Observer> observerList;

    private WeatherData weatherData;

    public WeatherSubject() {
        this.observerList = new ArrayList<>();
    }


    @Override
    public void registerObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObserver() {
        observerList.stream().forEach(observer -> observer.update(weatherData));
    }

    /**
     * 通过该方法将天气信息通知给所有的订阅者
     *
     * @param weatherData
     */
    public void publishWeather(WeatherData weatherData) {
        this.weatherData = weatherData;
        notifyObserver();
    }
}
```
> 订阅者,天气发布者会通知这个订阅者,让他来自己更新当前的天气信息
```java
/**
 * 当前的天气布告板
 *
 * @Author _G5niusX
 * @Date 2018/4/24 19:29
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {

    private Subject weatherSubject;
    private WeatherData weatherData;
    private String name;

    public CurrentConditionsDisplay(String name, Subject weatherSubject) {
        this.name = name;
        this.weatherSubject = weatherSubject;
        /*在进行实例化的时候,将这个订阅者注册到发布者里面*/
        weatherSubject.registerObserver(this);
    }

    @Override
    public void display() {
        String template = this.name + "播报当前的天气温度是: {0}°C, 湿度是: {1}%, 压强是: {2}Pa";
        System.out.println(MessageFormat.format(template, weatherData.getTemperature(), weatherData.getHumidity(), weatherData.getPressure()));
    }

    /**
     * 发布者会调用这个方法来通知订阅者天气数据的改变
     *
     * @param weatherData
     */
    @Override
    public void update(WeatherData weatherData) {
        this.weatherData = weatherData;
        display();
    }
}
```
##### 1.2 测试代码
```java
/**
 * @Author _G5niusX
 * @Date 2018/4/24 19:36
 */
@Log4j2
public class ObserverTest {

    private static final String[] NAMES = new String[5];

    @Before
    public void init() {
        for (int i = 0; i < NAMES.length; i++) {
            NAMES[i] = "CCTV " + (i + 1);
        }
    }

    @Test
    public void weatherPushObserverTest() {
        for (int i = 0; i < NAMES.length; i++) {
            /*先创建消息发布者*/
            WeatherSubject weatherSubject = new WeatherSubject();
            /*创建当前的天气,将它传递给订阅者*/
            WeatherData weatherData = new WeatherData();
            float x = new Random().nextFloat();
            x = (float) Math.round(x * 100);
            weatherData.setTemperature(x);
            weatherData.setHumidity(x);
            weatherData.setPressure(x);
            /*创建消息发布者,并且将这个订阅者注册到了发布者里面*/
            CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(NAMES[i], weatherSubject);
            /*订阅者发布了消息*/
            weatherSubject.publishWeather(weatherData);
        }
    }
}
```
#### 2 拉模型
> 由订阅者主动(pull)从发布者获取消息,核心是把整个主题对象都传递给了订阅者,让订阅者自己选择需要的信息
##### 2.1 java代码
> 天气主题对象
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/25 8:34
 */
@Log4j2
public class WeatherObservable implements PullSubject {
    /**
     * 标记数据是否改变
     */
    private boolean changed;
    /**
     * 存放观察者的数组
     */
    private Set<PullObserver> observerSet;

    /**
     * 温度
     */
    private float temperature;
    /**
     * 湿度
     */
    private float humidity;
    /**
     * 压力
     */
    private float pressure;

    public WeatherObservable() {
        observerSet = new HashSet<>();
    }

    @Override
    public void registerObserver(PullObserver observer) {
        observerSet.add(observer);
    }

    @Override
    public void removeObserver(PullObserver observer) {
        observerSet.remove(observer);
    }

    @Override
    public void notifyObserver() {
        if (!changed) {
            log.warn("数据状态没有发生改变....");
            return;
        }
        observerSet.stream().forEach(observer -> {
            observer.update(this, null);
        });
    }

    public void weatherChanged() {
        setChanged(true);
        notifyObserver();
    }

    public void setWeatherData(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        weatherChanged();
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

}
```
> 订阅者接口
```java
/**
 * @author SaErNuoSi
 * @date 2018/4/25 8:56
 */
@FunctionalInterface
public interface PullObserver {
    /**
     * 拉模型的重点,将整个对象都传递给观察者，让观察者来选择自己的数据
     *
     * @param weatherObservable
     * @param object
     */
    void update(WeatherObservable weatherObservable, Object object);
}
```
##### 2.2 测试代码
```java
/**
 * @Author _G5niusX
 * @Date 2018/4/24 19:36
 */
@Log4j2
public class ObserverTest {

    @Test
    public void weatherPullObserverTest() {
        WeatherObservable weatherObservable = new WeatherObservable();
        weatherObservable.registerObserver((weatherObserver, object) -> {
            float temperature = weatherObservable.getTemperature();
            System.out.println("CCTV播报温度:" + temperature);
        });
        weatherObservable.registerObserver((weatherObserver, object) -> {
            float pressure = weatherObservable.getPressure();
            System.out.println("CCTV2播报压强:" + pressure);
        });
        weatherObservable.setWeatherData(20, 40, 80);
    }
}
```
### 类图
![观察者模式类图](https://img-blog.csdn.net/20180425105315149?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1NhRXJOdW9TaQ==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
源码地址 [https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/observer](https://gitee.com/saernuosi/java-base/tree/master/design-pattern/src/main/java/com/desgin/pattern/observer)