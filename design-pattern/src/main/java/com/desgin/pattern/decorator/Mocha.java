package com.desgin.pattern.decorator;

import java.io.FileInputStream;
import java.math.BigDecimal;

/**
 * 摩卡
 *
 * @author SaErNuoSi
 * @date 2018/4/25 13:59
 */
public class Mocha extends CondimentDecorator {

    private AbstractBeverage abstractBeverage;

    public Mocha(AbstractBeverage abstractBeverage) {
        this.abstractBeverage = abstractBeverage;
    }

    @Override
    public String getDescription() {
        return "[摩卡]" + abstractBeverage.getDescription();
    }

    @Override
    BigDecimal coast() {
        return new BigDecimal(20).add(abstractBeverage.coast());
    }
}
