package com.desgin.pattern.commmand;

import lombok.extern.log4j.Log4j2;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 14:59
 */
@Log4j2
public class NoCommand implements Command{
    @Override
    public void execute() {
       log.info("什么都不干...");
    }

    @Override
    public void undo() {
        log.info("什么都不干...");
    }
}
