package com.desgin.pattern.iterator;

import java.util.List;

/**
 * @Author _G5niusX
 * @Date 2018/5/1 17:29
 */
public class PancakeHouseMenuIterator implements MenuIterator {

    List<MenuItem> pancakeHouseMenus;
    private int position;

    public PancakeHouseMenuIterator(List<MenuItem> pancakeHouseMenus) {
        this.pancakeHouseMenus = pancakeHouseMenus;
    }

    @Override
    public boolean hasNext() {
        if (position >= pancakeHouseMenus.size()) {
            return false;
        }
        return true;
    }

    @Override
    public Object next() {
        MenuItem pancakeHouseMenu = pancakeHouseMenus.get(position);
        position++;
        return pancakeHouseMenu;
    }
}
