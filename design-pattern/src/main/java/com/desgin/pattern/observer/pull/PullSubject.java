package com.desgin.pattern.observer.pull;

/**
 * @author SaErNuoSi
 * @date 2018/4/25 8:56
 */
public interface PullSubject {
    /**
     * 注册观察者
     */
    void registerObserver(PullObserver observer);

    /**
     * 移除观察者
     */
    void removeObserver(PullObserver observer);

    /**
     * 通知观察者
     */
    void notifyObserver();
}
