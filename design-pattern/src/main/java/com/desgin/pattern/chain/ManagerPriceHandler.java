package com.desgin.pattern.chain;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 16:44
 */
public class ManagerPriceHandler extends PriceHandler {
    public ManagerPriceHandler(String name) {
        super(name);
    }

    @Override
    void processPrice(float discount) {
        if (discount <= 0.10) {
            System.out.format("%s批准了%.2f折扣%n", name, discount);
        } else {
            nextPriceHandler.processPrice(discount);
        }
    }
}
