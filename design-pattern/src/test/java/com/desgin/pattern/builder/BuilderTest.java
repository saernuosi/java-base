package com.desgin.pattern.builder;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author SaErNuoSi
 * @date 2018/4/13 17:26
 */
public class BuilderTest {

    @Test
    public void builderTest() {
        Computer computer = new Computer.ComputerBuilder().withAudio("魔音音响").withCpu("i3")
                .withDisplay("dell").withGraphicsCard("英伟达").withMemory("2G")
                .build();
        Assert.assertNotNull(computer);
        System.out.println(computer);
    }
}
