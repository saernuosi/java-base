package com.desgin.pattern.commmand;

import lombok.extern.log4j.Log4j2;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 13:47
 */
@Log4j2
public class Light {
    public void on() {
        log.info("打开灯...");
    }

    public void off() {
        log.info("关闭灯...");
    }
}
