package com.desgin.pattern.iterator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author _G5niusX
 * @Date 2018/5/1 16:26
 */
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MenuItem {
    private String name;
    private String description;
    private double price;
}
