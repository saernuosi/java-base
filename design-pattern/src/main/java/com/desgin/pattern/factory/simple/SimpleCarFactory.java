package com.desgin.pattern.factory.simple;

import com.desgin.pattern.factory.Car;

/**
 * 简单工厂模式,通过判断来产生各种产品
 *
 * @author SaErNuoSi
 * @date 2018/4/23 12:38
 */
public class SimpleCarFactory {
    public static Car factory(String type) {
        switch (type) {
            case "SUV":
                return new SUVCar();
            case "SPECIAL":
                return new SpecialCar();
            default:
                return new Car() {
                    @Override
                    public String name() {
                        return "大众";
                    }

                    @Override
                    public int speed() {
                        return 200;
                    }
                };
        }
    }
}
