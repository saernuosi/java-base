package com.desgin.pattern.strategy;

/**
 * 玩具小黄鸭,不会飞行
 *
 * @author SaErNuoSi
 * @date 2018/4/24 9:58
 */
public class YellowDuck extends Duck {
    public YellowDuck() {
        this.setFlying(() -> {
            System.out.println("我不会飞行...");
        });
    }

    @Override
    public void display() {
        System.out.println("小黄鸭...一个小玩具");
    }
}
