package com.desgin.pattern.strategy;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 13:46
 */
public abstract class Duck {
    private Flying flying;

    /**
     * 鸭子的形状
     */
    public abstract void display();

    public void duck() {
        System.out.println("鸭子叫....");
    }

    public void setFlying(Flying flying) {
        this.flying = flying;
    }

    public void fly() {
        this.flying.fly();
    }
}
