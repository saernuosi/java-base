package com.desgin.pattern.commmand;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 13:44
 */
public interface Command {
    void execute();

    void undo();
}
