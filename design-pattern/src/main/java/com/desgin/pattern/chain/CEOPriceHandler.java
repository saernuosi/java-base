package com.desgin.pattern.chain;

/**
 * @Author _G5niusX
 * @Date 2018/4/22 16:46
 */
public class CEOPriceHandler extends PriceHandler {
    public CEOPriceHandler(String name) {
        super(name);
    }

    @Override
    void processPrice(float discount) {
        if (discount <= 0.20) {
            System.out.format("%s批准了%.2f折扣%n", name, discount);
        } else {
            System.out.format("%s拒绝%.2f折扣,原因是:折扣太高%n", name, discount);
        }
    }
}
