package com.desgin.pattern.factory;

import java.text.MessageFormat;

/**
 * @author SaErNuoSi
 * @date 2018/4/13 14:11
 */
public interface Car {
    String name();

    int speed();

    default String detail() {
        String template = "汽车名称:{0},速度:{1}km/h";
        return MessageFormat.format(template, name(), speed());
    }
}
