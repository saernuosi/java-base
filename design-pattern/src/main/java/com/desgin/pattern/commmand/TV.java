package com.desgin.pattern.commmand;

import lombok.extern.log4j.Log4j2;

import java.text.MessageFormat;

/**
 * @author SaErNuoSi
 * @date 2018/4/26 14:40
 */
@Log4j2
public class TV {
    private int voice;

    public void on() {
        log.info("打开电视...");
    }

    public void off() {
        log.info("关闭电视...");
    }

    public int getVoice() {
        return voice;
    }

    public void setVoice(int voice) {
        log.info(MessageFormat.format("音量由{0}变成{1}", this.voice, voice));
        this.voice = voice;
    }
}
